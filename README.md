# Ada Neuron Logo
---

![Ada Neuron Logo](adaNeuron.png "Logo Ada Neuron")
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Licencia Creative Commons Atribución-NoComercial-SinDerivadas 4.0 Internacional</a>.

Ada Neuron Logo por [Gabriel Vargas Monroy](https://gitlab.com/vmgabriel "Primer Creador") y [Daissi Bibiana Gonzales Roldan](http://gitlab.com/Timdx "Segundo Creador"), Esta lanzado bajo la licencia [Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/ "Licencia Logo").
